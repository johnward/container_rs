//use nix::sched::{unshare, CloneFlags};
use nix::sched::{unshare, CloneFlags};
use nix::sys::wait::waitpid;
use nix::unistd::{fork, getpid, sethostname, ForkResult};
use std::process::exit;

fn main() {
    println!("Parent [{:?}] - starting the container...", getpid());

    match unsafe { fork() } {
        Ok(ForkResult::Parent { child, .. }) => {
            println!(
                "Parent [{:?}] - created child process {:?}",
                getpid(),
                child
            );
            // Wait for the child process to finish
            waitpid(child, None).expect("Failed to wait for the child process");
            println!("Parent [{:?}] - container finished", getpid());
        }
        Ok(ForkResult::Child) => {
            // Enter new namespaces to create an isolated environment for the container
            unshare(CloneFlags::CLONE_NEWUTS | CloneFlags::CLONE_NEWPID)
                .expect("Failed to enter new namespaces");

            println!("Container [{:?}] - inside the container!", getpid());
            sethostname("container").expect("Failed to set the container's hostname");

            // Execute the target program within the container
            nix::unistd::execvp(
                &std::ffi::CString::new("/bin/sh").unwrap(),
                &[std::ffi::CString::new("/bin/sh").unwrap()],
            )
            .expect("Failed to execute the target program");

            exit(0);
        }
        Err(_) => {
            eprintln!("Parent [{:?}] - failed to fork", getpid());
            exit(1);
        }
    }
}
